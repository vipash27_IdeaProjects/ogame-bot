package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import main.Program;
import user.User;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class UserDataController implements Initializable{

    private ObservableList<String> listSystem;

    public Parent loadUserDataWindow() {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/fxml/UserDataWindow.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parent;
    }

    @FXML
    private Label labelError;

    @FXML
    private TextField textFieldEmail;

    @FXML
    private PasswordField passwordField;

    @FXML
    private ComboBox comboBoxSystem;

    @FXML
    private Button buttonNext;

    public void clickButtonNext() {
        System.out.println("clickButtonNext");
        if (displayErrorMessage())
        {
            setDataUser();
        }
    }

    public void initialize(URL location, ResourceBundle resources) {
        listSystem = FXCollections.observableArrayList("1. Вселенная", "Antares", "Betelgeuse", "Cygnus", "Deimos");
        comboBoxSystem.setItems(listSystem);
        comboBoxSystem.setPromptText(listSystem.get(0));
    }

    private boolean displayErrorMessage() {
        if (textFieldEmail.getText().equals("") || passwordField.getText().equals(""))
        {
            labelError.setText("* All fields are required!");
            return false;
        }
        else
        {
            labelError.setText("");
            return true;
        }
    }

    private void setDataUser() {
        Program.getUser().setEmail(textFieldEmail.getText());
        Program.getUser().setPassword(passwordField.getText());
        Program.getUser().setSystem(comboBoxSystem.getPromptText());
        BrowserSelectionController bsc = new BrowserSelectionController();
        Program.loadNewScene(bsc.loadBrowserSelectionWindow());
    }
}
