package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import main.Program;
import model.chrome.ChromeModel;

import java.io.IOException;

public class BrowserSelectionController {

    public Parent loadBrowserSelectionWindow() {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/fxml/BrowserSelectionWindow.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parent;
    }

    @FXML
    private Button buttonChrome;

    @FXML
    private Button buttonFirefox;

    @FXML
    private Button buttonOpera;

    public void clickButtonChrome() {
        ChromeModel chromeModel = new ChromeModel();
        chromeModel.login();
    }

    public void clickButtonFirefox() {
        System.out.println("clickButtonFirefox");
    }

    public void clickButtonOpera() {
        System.out.println("clickButtonOpera");
    }

    public void clickLabelBack(MouseEvent mouseEvent) {
        UserDataController udc = new UserDataController();
        Program.loadNewScene(udc.loadUserDataWindow());
    }

}
