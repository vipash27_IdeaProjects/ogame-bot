package model.chrome;

import main.Program;
import model.chrome.actions.FindUser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class ChromeModel {

    private WebDriver driver = null;

    public void login() {
        setDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        openMainPageOgame();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        clickButtonLogin();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        setUserData();
        clickButtonEntrance();
    }

    private void setDriver() {
        System.setProperty("webdriver.chrome.driver", "E:/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    private void openMainPageOgame() {
        driver.get("https://ru.ogame.gameforge.com/");
    }

    private void clickButtonLogin() {
        WebElement element = driver.findElement(By.id("loginBtn"));
        element.click();
    }

    private void setUserData() {
        WebElement elementEmail = driver.findElement(By.className("js_userName"));
        elementEmail.sendKeys(Program.getUser().getEmail());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement elementPassword = driver.findElement(By.id("passwordLogin"));
        elementPassword.sendKeys(Program.getUser().getPassword());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement elementSystem = driver.findElement(By.id("serverLogin"));
        elementSystem.sendKeys(Program.getUser().getSystem());
    }

    private void clickButtonEntrance() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement element = driver.findElement(By.id("loginSubmit"));
        element.click();

        FindUser findUser = new FindUser();
        findUser.findUser(driver, "Jobs");
    }
}
