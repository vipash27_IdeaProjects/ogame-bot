package model.chrome.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class FindUser {

    private Map<String, String> listAllNames = new HashMap<String, String>();

    public void findUser(WebDriver driver, String userName) {
        clickMenuButtonGalaxy(driver);
        goBySystems(driver, listAllNames);

        for (Map.Entry<String, String> map : listAllNames.entrySet())
        {
            System.out.println(map.getKey() + " " + map.getValue());
        }
        System.out.println("========================================================================");

        findSpecificUser(listAllNames, userName);
    }

    private void clickMenuButtonGalaxy(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement element = driver.findElement(By.xpath("//*[@id=\"menuTable\"]/li[9]/a"));
        element.click();
    }

    private void goBySystems(WebDriver driver, Map<String, String> listAllNames) {
        for (int galaxy = 1; galaxy < 10; galaxy++)
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebElement elementGalaxyInput = driver.findElement(By.id("galaxy_input"));
            elementGalaxyInput.sendKeys("" + galaxy + "");
            for (int system = 1; system < 500; system++)
            {
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                WebElement elementSystemInput = driver.findElement(By.id("system_input"));
                elementSystemInput.sendKeys("" + system + "");

                WebElement elementButtonSystemNext = driver.findElement(By.className("btn_blue"));
                elementButtonSystemNext.click();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                String location = galaxy + ":" + system;
                getListAllNames(driver, location);
            }
        }
    }

    private void getListAllNames(WebDriver driver, String location) {
        List<WebElement> listStatusTarget = driver.findElements(By.className("status_abbr_honorableTarget"));

        for (int i = 0; i < listStatusTarget.size(); i++)
        {
            if (listStatusTarget.get(i).getText().equals("пп") || listStatusTarget.get(i).getText().equals(""))
            {
                listStatusTarget.remove(i);
            }
            else
            {
                listAllNames.put(listStatusTarget.get(i).getText(), location);
            }
        }
    }

    private void findSpecificUser(Map<String, String> listAllNames, String userName) {
        for (Map.Entry<String, String> map : listAllNames.entrySet())
        {
            if (map.getKey().equals(userName))
            {
                System.out.println(map.getKey() + " " + map.getValue());
            }
        }
    }
}
