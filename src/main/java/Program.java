import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Program {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","./src/main/resources/chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("https://ru.ogame.gameforge.com/");

        WebElement elementLoginBtn = driver.findElement(By.id("loginBtn"));
        elementLoginBtn.click();

        WebElement elementUserName = driver.findElement(By.className("black-border"));
        elementUserName.sendKeys("Forrest");
        //elementUserName.submit();

    }
}
