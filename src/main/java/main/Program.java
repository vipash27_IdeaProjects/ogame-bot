package main;

import controller.UserDataController;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import user.User;

public class Program extends Application {

    private static User user;

    public static User getUser() {
        return user;
    }

    private static Scene scene = null;

    public static void loadNewScene(Parent root){
        if (scene == null)
        {
            scene = new Scene(root);
        }
        else
        {
            scene.setRoot(root);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        user = new User();
        UserDataController udc = new UserDataController();
        loadNewScene(udc.loadUserDataWindow());
        primaryStage.setTitle("Ogame");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
